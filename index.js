const express = require('express');
const { dbConection } = require('./database/config');
const cors = require('cors');
require('dotenv').config();

//Crear el servidor de express
const app = express();

//Base de datos
dbConection();

//Cors
app.use(cors());

//Directorio publico
app.use(express.static('public'));

//Rutas
//auth, crear, login, renew
//CRUD: eventos

app.use(express.json());

app.use('/api/auth', require('./routes/auth'));
app.use('/api/events', require('./routes/events'));
app.use('/api/groups', require('./routes/groups'));

app.listen(process.env.PORT, () => {
  console.log(`Servidor corriendo en puerto ${process.env.PORT}`);
});
