const { Router } = require('express');
const { check } = require('express-validator');
const { getGroups, createGroup } = require('../controllers/groups');
const { validarJWT } = require('../middlewares/validar-jwt');
const { validatorMiddleware } = require('../middlewares/validationMiddleware');
const router = Router();

router.use(validarJWT);
router.get('/', getGroups);

router.post(
  '/',
  [
    check('name', 'El nombre es obligatorio').not().isEmpty(),
    validatorMiddleware,
  ],
  createGroup
);
module.exports = router;
