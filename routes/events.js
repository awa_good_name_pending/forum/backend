const { Router } = require('express');
const { check } = require('express-validator');
const {
  getEvents,
  createEvent,
  updateEvent,
  deleteEvent,
} = require('../controllers/events');
const { isDate } = require('../helpers/isDate');
const { validarJWT } = require('../middlewares/validar-jwt');
const { validatorMiddleware } = require('../middlewares/validationMiddleware');
const router = Router();

// Todas las rutas tienen el validar jwt

router.use(validarJWT);

// Obtener eventos
router.get('/', getEvents);

// Crear un nuevo evento
router.post(
  '/',
  [
    check('title', 'El titulo es obligatorio').not().isEmpty(),
    check('start', 'Fecha de inicio es obligatoria').custom(isDate),
    check('end', 'Fecha de finalizacion es obligatoria').custom(isDate),
    validatorMiddleware,
  ],
  createEvent
);

// Actualizar evento
router.put(
  '/:id',
  [
    check('title', 'El titulo es obligatorio').not().isEmpty(),
    check('start', 'Fecha de inicio es obligatoria').custom(isDate),
    check('end', 'Fecha de finalizacion es obligatoria').custom(isDate),
    validatorMiddleware,
  ],
  updateEvent
);

//Elminar evento
router.delete('/:id', deleteEvent);

module.exports = router;
