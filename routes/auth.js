const { Router } = require('express');
const { check } = require('express-validator');
const {
  crearUsuario,
  loginUsuario,
  revalidarToken,
} = require('../controllers/auth');
const { validarJWT } = require('../middlewares/validar-jwt');
const { validatorMiddleware } = require('../middlewares/validationMiddleware');
const router = Router();

router.post(
  '/new',
  [
    check('name', 'El nombre es obligatorio').not().isEmpty(),
    check('email', 'El email es obligatorio').isEmail(),
    check('password', 'El password debe de ser de 6 caracteres').isLength({
      min: 6,
    }),
    validatorMiddleware,
  ],
  crearUsuario
);

router.post(
  '/',
  [
    check('email', 'The email is required').isEmail(),
    check('password', 'The password must have at least 6 characters').isLength({
      min: 6,
    }),
    validatorMiddleware,
  ],
  loginUsuario
);

router.get('/renew', validarJWT, revalidarToken);

module.exports = router;
