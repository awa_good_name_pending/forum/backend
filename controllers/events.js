const Evento = require('../models/Evento');

const getEvents = async (req, res) => {
  const eventos = await Evento.find().populate('user', 'name');
  res.json({
    ok: true,
    eventos,
  });
};

const createEvent = async (req, res) => {
  const evento = new Evento(req.body);

  try {
    evento.user = req.uid;

    const eventoDB = await evento.save();

    res.json({
      ok: true,
      evento: eventoDB,
    });
  } catch (error) {
    console.log(error);
    res.status(500).json({
      ok: false,
      msg: 'Hable con el administrador',
    });
  }
};

const updateEvent = async (req, res) => {
  const eventoId = req.params.id;

  try {
    const evento = await Evento.findById(eventoId);

    if (!evento) {
      return res.status(404).json({
        ok: false,
        msg: 'No hay un evento con ese id',
      });
    }

    if (evento.user.toString() !== req.uid) {
      return res.status(401).json({
        ok: false,
        msg: 'No tiene los permisos necesarios',
      });
    }

    const eventoUpdate = {
      ...req.body,
      user: req.uid,
    };

    const updated = await Evento.findByIdAndUpdate(eventoId, eventoUpdate, {
      new: true,
    });

    res.json({
      ok: true,
      evento: updated,
    });
  } catch (error) {
    console.log(error);
    res.status(500).json({
      ok: false,
      msg: 'Hable con el administrador',
    });
  }
};

const deleteEvent = async (req, res) => {
  const eventId = req.params.id;

  try {
    const event = await Evento.findById(eventId);

    if (!event) {
      return res.status(404).json({
        ok: false,
        msg: 'El evento no existe con ese id',
      });
    }

    if (event.user.toString() !== req.uid) {
      return res.status(401).json({
        ok: false,
        msg: 'No tiene los permisos necesarios',
      });
    }

    await Evento.findByIdAndRemove(eventId);

    res.json({
      ok: true,
    });
  } catch (error) {
    console.log(error);
    res.status(500).json({
      ok: false,
      msg: 'Hable con el administrador',
    });
  }
};

module.exports = {
  getEvents,
  createEvent,
  updateEvent,
  deleteEvent,
};
